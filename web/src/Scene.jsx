import React, {Suspense} from 'react'
import * as THREE from 'three'
import {
  Text3D
} from '@react-three/drei'
import { Outlet, Link, useNavigate } from 'react-router-dom'
import { useFrame, Canvas } from '@react-three/fiber'
import styled from 'styled-components'

const Nav = styled.nav`
border-bottom: solid 1px;
padding-bottom: 1rem;
`
function Scene() {
  const navigate = useNavigate()
  return (
    <Canvas
      style={{
        width: '100vw',
        height: '10vh'
      }}
      >
      <Suspense fallback={null}>

      </Suspense>
    </Canvas>
  )
}

export default Scene
