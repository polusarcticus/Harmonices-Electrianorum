import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import App from "./App";
import About from './routes/About'

import Pyramidis from './routes/Pyramidis/Pyramidis'

import Musicae from './routes/Musicae/Musicae'
import Chordae from './routes/Musicae/Chordae'
import Septimae from './routes/Musicae/Septimae'
import Cellae from './routes/Musicae/Cellae'
import Wildbergicarum from './routes/Musicae/Wildbergicarum'
import Cubitus from './routes/Musicae/Cubitus'
import Ptolemicarum from './routes/Musicae/Ptolemicarum'
import Utriculus from './routes/Musicae/Utriculus'
import Undecimae from './routes/Musicae/Undecimae'
import YoWayYo from "./routes/Musicae/YoWayYo";

import LinguaeLatinae from './routes/LinguaeLatinae/LinguaeLatinae'
import LinguamLatensisStudeo from './routes/LinguaeLatinae/LinguamLatensisStudeo'
import Keppleri from './routes/LinguaeLatinae/Keppleri'
import Gauss from './routes/LinguaeLatinae/Gauss'
import JoannePoleno from './routes/LinguaeLatinae/JoannePoleno/JoannePoleno'
import { VorticibusCoelestibus } from "./routes/LinguaeLatinae/JoannePoleno/VorticibusCoelestibus";
import Fluctibus from './routes/LinguaeLatinae/Fluctibus/Fluctibus'
import Utriusque from './routes/LinguaeLatinae/Fluctibus/Utriusque'
import Blanchini from './routes/LinguaeLatinae/Blanchini/Blanchini'
import TribusGeneribus from './routes/LinguaeLatinae/Blanchini/TribusGeneribus'
import { Breganio } from "./routes/LinguaeLatinae/Breganio/Breganio";
import { TheologiaeGentium } from "./routes/LinguaeLatinae/Breganio/TheologiaeGentium";
import Geometriae from './routes/Geometriae/Geometriae'
import IoannisDee from './routes/Geometriae/IoannisDee'
import FranciscoFlussateCandalla from './routes/Geometriae/FranciscoFlussateCandalla'
import VorticibusRodin from './routes/Geometriae/VorticibusRodin'
import { Kircheri } from "./routes/LinguaeLatinae/Kircheri/Kircheri";
import OedipusAegyptiacus from "./routes/LinguaeLatinae/Kircheri/OedipusAegyptiacus";

import Cartographica from './routes/Cartographica/Cartographica'
import Mercator from './routes/Cartographica/Mercator/Mercator'
import Ortelius from "./routes/Cartographica/Ortelius/Ortelius";
import { Theatrum } from "./routes/Cartographica/Ortelius/Theatrum/Theatrum";
import { Tartariae } from "./routes/Cartographica/Ortelius/Theatrum/Tartariae/Tartariae";
import { WiktionaryRedirector } from "./components/WiktionaryRedirector";

import { createRoot } from 'react-dom/client';


const container = document.getElementById('root')
const root = createRoot(container)

root.render(

  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />}>
        <Route path ="/about" element={<About />} />
        <Route path ="/Pyramidis" element={<Pyramidis />} />
        <Route path ="/Musicae" element={<Musicae />}>
          <Route path="Chordae" element={<Chordae />}>
            <Route path="Septimae" element={<Septimae />} />
            <Route path="Undecimae" element={<Undecimae />} />
            <Route path="Cubitus" element={<Cubitus />} />
            <Route path="Wildbergicarum" element={<Wildbergicarum />} />
            <Route path="Cellae" element={<Cellae />} />
            <Route path="Ptolemicarum" element={<Ptolemicarum />} />
            <Route path="Utriculus" element={<Utriculus />} />
            <Route path="YoWayYo" element={<YoWayYo />} />
          </Route>
        </Route>
        <Route path ="/Linguae-Latinae" element={<LinguaeLatinae />}>
          <Route path="Keppleri" element={<Keppleri />} />
          <Route path="Gauss" element={<Gauss />} />
          <Route path="Joanne-Poleno" element={<JoannePoleno />}>
            <Route path="VorticibusCoelestibus" element={<VorticibusCoelestibus/>} />
          </Route>
          <Route path="Fluctibus" element={<Fluctibus />}>
            <Route path="Utriusque" element={<Utriusque />} />
          </Route>
          <Route path="Blanchini" element={<Blanchini />}>
            <Route path="Tribus-Generibus" element={<TribusGeneribus />} />
          </Route>
          <Route path="Breganio" element={<Breganio />}>
            <Route path="TheologiaeGentium" element={<TheologiaeGentium />} />
          </Route>
          <Route path="Kircheri" element={<Kircheri />}>
            <Route path="OedipusAegyptiacus" element={<OedipusAegyptiacus />} />
          </Route>
        </Route>
        <Route path ="/Geometriae" element={<Geometriae />}>
          <Route path="Ioannis-Dee" element={<IoannisDee />} />
          <Route path="Francisco-Flussate-Candalla" element={<FranciscoFlussateCandalla />} />
          <Route path="Vorticibus-Rodin" element={<VorticibusRodin />} />
        </Route>
        <Route path="/Cartographica" element={<Cartographica />}>
          <Route path="Mercator" element={<Mercator />} />
          <Route path="Ortelius" element={<Ortelius />}>
            <Route path="theatrum-orbis-terrarum" element={<Theatrum />}>
              <Route path="tartariae" element={<Tartariae />} />
            </Route>
          </Route>
        </Route>
      </Route>
      <Route path="/Studeo" element={<LinguamLatensisStudeo />} />
      <Route path ="/wiki/*" element={<WiktionaryRedirector />} />
    </Routes>
  </BrowserRouter>
)
