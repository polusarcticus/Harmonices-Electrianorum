import { useState, useEffect, useCallback  } from 'react';
import { Link, Outlet  } from "react-router-dom";
import axios from 'axios';
import styled from 'styled-components'
import parse from 'html-react-parser'
const Title = styled.h1`
  grid-column: 1/-1;
  grid-row: 1;

`
const TextBox = styled.textarea`
  grid-column: 1;
  grid-row: 2;
  width: 33vw;
  height: 33vh;
`
const Submit = styled.input`
  grid-column: 1;
  grid-row: 3;
`

const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 1fr 33vh 1fr;
`
export default function LinguamLatensisStudeo() {
  const [text, setText] = useState('')
  const [response, setResponse] = useState('')

  return (<Container>
    <Title>Instrumentum ad linguam Latinam</Title>
    <TextBox
      name="words"
      value={text}
      onChange={() => { setText(event.target.value)}}
    />
    <Submit type="submit" value="Inspectio" onClick={() => {
      console.log(text.split())
      Promise.all(
        text.split().map(async (word) => {
          const res = await axios({
            method: 'get',
            url: `https://en.wiktionary.org/w/api.php?action=parse&page=${text}&prop=text&formatversion=2&origin=*&format=json`,
          })


        })
      )
      axios({
        method: 'get',
        url: `https://en.wiktionary.org/w/api.php?action=parse&page=${text}&prop=text&formatversion=2&origin=*&format=json`,
      }).then((response) => {
        console.log(response)
        setResponse(response.data.parse.text)
      })
    }} />
    {parse(response)}
  </Container>)
}
