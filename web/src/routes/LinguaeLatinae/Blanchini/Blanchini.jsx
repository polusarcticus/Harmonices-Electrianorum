import { Link, Outlet  } from "react-router-dom";

export default function Blanchini() {
  return (<>
    <h3>Francisci Blanchini</h3>
    <Link to="Tribus-Generibus">Tribus Generibus Instrumentorum musicae veterum organicae</Link>
    <Outlet />
  </>)
}
