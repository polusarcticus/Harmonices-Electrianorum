import { Fragment } from "react";
import { Link, Outlet  } from "react-router-dom";
import { useState, useEffect } from "react";
import Wiktionary from '@/components/Wiktionary'

import styled from 'styled-components'

export default function TribusGeneribus() {
  const [wikiText,setWikiText] = useState("")
  const [x,setX] = useState(0)
  const [y,setY] = useState(0)
  const sistrumText = (<div><p><a target="_blank" href="https://archive.org/details/francisciblanchi0000bian/page/55/mode/1up?view=theater">
      12. Sistrum</a> Celebre illud Isidis Organum, & Isiacorum gestamen, quod ab Apulejo adamussim delineatur lib. Xi. Metam. observante Pignorio de Mensa Isiaca pag. 34.</p>
      <p>Aereum scilicet crepitaculum, cujus perangustam laminam, in modum baltei recurvatam, trajectae mediae parvae virgulae crispante brachio tergeminos ictus reddebant, argutum, sonorum, & gravem</p>
      <p>Virgulae per laminam trajectae, modo tres, modo quatuor, non fine mysterio, visuntur in veteribus monumentis Sistrum exprimentibus; ut eruditissimus Fabrettus probat Inscript. cap.6.p.489.</p>
      <p>Duas ibi figuras Sistri, ex Musaeo Strozziano, & gaza Medicea ad magnitudinem prototyporum, exactas, exhibet, unde hanc mutuamur quadrichordem.</p>
      <p>Sistrum putant communiter esse cymbalum alarum Isa.c.18.</p>
      </div>)

  const UtriculusText = (<div><a target="_blank" href="https://archive.org/details/francisciblanchi0000bian/page/11/mode/1up?view=theater">Tibiae Utriculariae</a>
  <p>sive Utriculares, quae nostris pastoribus, & messoribus passim adhibentur.</p>
      </div>)


  return (<div>
    <p><a target="_blank" href="https://archive.org/details/francisciblanchi0000bian/mode/1up?view=theater&q=utriculus">archivo</a></p>
    <h3>Francisci Blanchini</h3>
    {sistrumText}
    <br />
    {UtriculusText}
    <Wiktionary/>
  </div>)
}
