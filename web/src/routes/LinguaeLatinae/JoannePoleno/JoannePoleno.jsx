
import { Link, Outlet  } from "react-router-dom";
import { VorticibusCoelestibus } from "./VorticibusCoelestibus";
export default function JoannePoleno() {
  return (<>
  <h3>Johanne Poleno</h3>
  <Link to="VorticibusCoelestibus">Vorticibus Coelestibus</Link>
  <Outlet />
  </>)
}
