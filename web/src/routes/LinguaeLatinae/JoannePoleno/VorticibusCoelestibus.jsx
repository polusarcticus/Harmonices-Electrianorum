import PDF from './De_vorticibus_coelestibus_dialogus_Cui_a.pdf'

import Wiktionary from '@/components/Wiktionary';
export const VorticibusCoelestibus = () => {
    return (<div>
    
    <a href={PDF} target="_blank">Archivo</a>
    <p>pg. 20</p>
    <h3>De Vorticibus Coelestibus Dialogus.</h3>
    <p>Diu optata illuxit tandem dies haec, qua, prout propositum jam fuit, de Vorticum Coelestium systemate, inter nos colloquamur. Quoniam vero plures circa idem propositiones jam in variis colloquiis anteacto tempore habitis, nullo servato ordine constituimus, in eo operam nostram in praestentia conferre debenus, ut easdem, novasque invicem, (quam aptius fieri continget) componentes.</p>
    <Wiktionary />
    </div>)
}