import { Link, Outlet  } from "react-router-dom";

export default function Fluctibus() {
  return (<>
      <h2>Roberto Fludd</h2>
      <Link to="Utriusque">Utriusque Cosmi</Link>
      <Outlet />
    </>)
}
