export default function Utriusque() {
  return (<>
    <h3><a target="_blank" href="https://archive.org/details/utriusquecosmima01flud/mode/1up?view=theater">Utriusque Cosmi</a></h3>
    <h4>Maioris scilicet et minoris metaphysica, physica atque technica</h4>
    <h2>Historia</h2>
    <h4>In duo Volumina secundum Cosmi differentiam diuisa</h4>
    <h4><i><b>Authore Roberto Flud</b> alias de Fluctibus, Armigero. & in Medicina Doctore Oxoniensi</i></h4>
    <h3>Tomus Primus</h3>
    <h4></h4>
    </>)
}
