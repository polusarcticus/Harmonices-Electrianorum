
import { Link, Outlet  } from "react-router-dom";
import Wiktionary from "@/components/Wiktionary";
export default function OedipusAegyptiacus() {
  return (<>
    <p><a target="_blank" href="https://archive.org/details/AthanasiiKircheriOedipusAegyptiacusVolI1652/page/n7/mode/1up">archivo</a></p>
    <p><a target="_blank" href="https://archive.org/details/AthanasiiKircheriOedipusAegyptiacusVolI1652/page/n127/mode/1up?view=theater">pg. 59 </a></p>
    <h4>Syntag. I. Chorographia Aegypti Numus V. Onuphis Id Est</h4>
    <i>Praeforturo Onuphis, </i>
    <p>Praefectura Onuphis, quintum in ordine Nomorum locum obtinet</p>
    <Wiktionary />
    </>)
}
