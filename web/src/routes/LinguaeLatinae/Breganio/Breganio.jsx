import { Link, Outlet  } from "react-router-dom";

export const Breganio = () => {
    return (
        <div>
            <h3>Raymund Breganius</h3>
            <Link to="TheologiaeGentium">Theologiae gentium de cognitione divina enarrationes V, quibus tota cognitionis series</Link>
            <Outlet />
        </div>
    )
}