
export const TheologiaeGentium = () => {
    return (<>
    <h3>Theologiae Gentium</h3>
    <p><b>P. 34</b></p> 
    <h4>Theologiae gentium de cognitione divina</h4>
    <h5>Enarratio secunda</h5>
    <p>De origine & successione cognitionis divinae in gentes; ipsisque; gentium doctoribus</p>
    <h4>F. Raymundo Breganio</h4>
    <h5>Ripano Auctore</h5>
    <br />
    <p><i>Ab Adam in Mathusalem, Noe, Abraham, & posteros propagatam fuisse veram sapientiam, narrat ex Augustino Steucho</i></p>
    <p>Cap. 1</p>
    <p>Sa tis rationabiliter Augustinus Steuchus lib.1.de perenni philosophia cap.1 autumat ab adam processise sapientiam in posteros, cum ipse sapientissiumus extiterit, utpote createus a domino: Qui, ut docet d.Thom.1.p.q 94.art 3.sic institutus est a Deo, ut habert omnium scientiam, in quibus homo natus est instrui;& de supernaturalibus tantam cognitionen accepit, quantu erat necessaria ad gubernationem vitae humanae secundum statum illum.</p>
    <p>Post diluvium vero propagatam fuisse doctrinam per Noe in filios suos;& posteros, idem Steuchus affirmat; tametsi huic assertioni assentire non videatur D. Augustinus, qui lib 16. de Ciuit. Dei cap.1.ait post diluvuium procurrentis sanctae vestigia ciuitatis utrum continuata sint, an intercurrentibus impietatis interrupta temporibus; ita ut nullus hominum veri unius Dei cultor existeret, ad liquidu scripturuis loquentibus inuenire difficile est: prpterea quia in canonicis libris post Noe, qui cum coniuge, actribus filijs, totidemque; nuribus suis meruit per arcam a vastatione diluvii liberati, non inuenimus usque; ad Abram ciuusquam pietatem cuidenti diuino eloquio praedicatam At nihilominus saltem </p>
    <br />
    <p><b>Pg. 77</b></p>
    <p><i>De Theologis Poetis, Philosophis, alijssque; rerum divinorum, apud Gentes, Doctoribus, & Scriptoribus, ac Demonum oraculis</i></p>
    <p><i>Cap. XX</i></p>
    <p>Multi profecto inuenti sunt Gentium doctores, ut ex Philosopho rum Serie, quam preposuimus, videre licet: sed nihilominus, ut eos omnio, a quibus divinorum cognitio manauit, non sileamus, repetendi ii erunt, qliique; memorandi, si qui rebus divinis quoquo modo tractauerunt.</p>
    <p>Fuere in primis Gymnosophistae, de quibus D. Hieronym lib2 con. louinianum consulendus est.</p>
    <p>Horum princeps larchas nominabatur, eo tempore, quo Apoll. Thyaneus discendi, visendique; studio ad eos se constulerat</p>
    <p>Post Druidas commemorat Laeit lib 1 de quibus ait Cesar lib o com druide tebus divinis intersunt, sacrificia publica, & privata procurant, religiones interpretantur</p>
    <p>Hi quoque: fere de omnibus controuersiis publicis, priuatisque; constituunt.</p>
    <p>Quam eorum religionem (inquit Suetonius) sub Augusto interdictam Claudius Imperator quintus penitus aboleuit</p>
    <p>Terio Magi quorum principum fuisse Zoroastrem persen memoriae proditum est; qui & librum, quem sacrum inscriptsit, aedidit</p>
    <p>Huius Zoroastris dogmata Sacerdotis Aegyptiorum susceperunt magnamque; ex his gloriam, quaesierunt, ut testatur Steuchus lib I cap 25 de perenni philosophia. Ab his usque; ad excidium troiae annos quinquies mille computat Hermodotus Platonicus in lib de disciplini, cum tame a creatione mundi usque; ad Ilij captiuitatem, anni circiter 4000. numerentur, ut ex Eusebio in Chronicis habetur; unde magorum vetustas crassum Hermodoti errorem comitatur, eandem opinionem Steuchus lib 1 de peeienni philosophia cap 25 tribuit plutarcho, scribens zoroastrem quinque annorum millia bellum troianum antecessisse, quad sicut forte incredibile videatur inquit, certe antiquissimus est omnium, qui vel in philosophia, vel legibus latis celebres sunt. Post Magos, plurimos sibi inuicem successisse refert Laertius lib 1 Hosthanas, Astrapsycos, Cobrias, ac Pazatas, donec ab Alexandrio euersum est persarum regnum. Phoenices quoque; multa docuerut ad Deos, Deorumque, cultum spectantia, quae memorant Sachoniatho Berutius, Philobiblius, & thaionis filius, apud Eusebium. Postremo Aegyptij; quorum princeps, ut diximus, fuit Mercurious Trismegistus, ex cuius Mercurialibus libris I'ythim quem prophetam aegyptium dicunt, multa rerum divinatum explicasle lamblichus testatur. Scriptores vero precipui & doctores Aegyptiorum notatur epies Deorum interpres, & sacrarum literarum scriba, manethus, ac meneteus, seleucus, iamblichus, anebon sacerdos eius preceptor & abamon anebonis magister, quorum meminit marsilius Ficinus in argum in lib Iamblichi Fuerunt preterea theologi poetae orpheus, musaeus, linus ut scribit D Augst lib 18 de ciuit cap 15</p>
    <br/>
    <p><b>pg.92</b></p>
    <p><i>Moyses in omni sapientia Aegyptiorum, & erat potens in uerbis, & in operibus suis</i></p>
    <p>Et Philo in primo de Vita Mosis libro affirmat Mosem eadem Aegyptiorum sapientia plenissime institutum maxime autem accepisse ab Aegyptijs doctoribus occultam philosophiam descriptiam literis, ut vocant, hieroglyphicis, hoc est, notis animalim quae ipsi venerantur pro num inibus. Ex propinquis autem regionibus euocati Chaldaei docuerunt syderalem scientiam: hanc Moses, & ab Aegyptijs didicit</p>
    <p>At D. lustinus martyr in lib quaestionum a gentibus positarum ait, mosem, omisso studio omnium Mathematicarum, solum adicecisse animum ad Hieroglyphicase disciplinas, quae tum apud Aegyptios solae in honore & pretio erant</p>
    <br/>
    <p><b>pg.94</b></p>
    <p><i>De mystica,& naturali Aegyptiorum Theologica, quam Eusebius, ex Manetho, Diodoro, Plutarcho, & Porphyrio excerpsit. CAp. VI</i></p>
    <br/>
    <p><b>pg. 101</b></p>
    <p><i>De Atlantiorum, Phrygum, & diuersa aliarum gentium theologia, ex eusebio, alijssque complurimis auctoribus Capt. VIII</i></p>
    <p>Post mortum autem Hyperionis, celi filios regnum inter se partitos fuisse, quorum clarissimi Atlas, & Saturnus fuerunt; Atlanti vicinas Oceano partes contigisse, & multam Astrologiae operam datam ,septemque, filias natas, quae Atlantides appellantur, a quibus quamplures Dij, & Heroes nati, & seniore ipsarum Mai loui coniuncta, Mercurium procreatum Saturnum quoque Atlantis filium auritia, & impietate predium, sororem Cybelem duxiste, q qui Iouem suscepit, quamuis & alium Iouem celi fratrem, & cretae Regem contendunt fuisse, qui multo inferior posteriore ioue fuit, quie totins orbis imperium primus, ac fere solus obtinuit</p>
    </>)
}