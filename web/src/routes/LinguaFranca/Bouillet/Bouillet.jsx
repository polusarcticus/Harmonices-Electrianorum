import { Link, Outlet  } from "react-router-dom";

export default function Bouillet() {
  return (<>
    <h3>Marie Nicolas Bouillet</h3>
    <Link to="AtlasUniverselHistoire">Atlas Universel d'histoire et de Geographie</Link>
    <Outlet />
  </>)
}
