import { useState, useEffect, useCallback } from 'react'
import { Link, Outlet  } from "react-router-dom";
export const Theatrum = () => {
    return (
        <>
        <h2>Theatrum Orbis Terrarum</h2>
        <h3><a target="_blank" href="https://archive.org/details/theatrumorbister00orte/page/n3/mode/2up">(PDF) Theatrum Orbis Terrarum</a></h3>
        <Link to="tartariae">Tartariae</Link>
        <Outlet />
        </>
    )
}