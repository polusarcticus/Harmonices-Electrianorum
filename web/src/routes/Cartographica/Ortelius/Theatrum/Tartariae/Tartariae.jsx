import Wiktionary from '@/components/Wiktionary';
import map from './10000103.jpg'
export const Tartariae = () => {
    return (
        <>
        <p>pg. 205</p>
        <h3><a target="_blank" href="https://archive.org/details/theatrumorbister00orte/page/n203/mode/2up">(PDF) Theatrum Orbis Terrarum</a></h3>
        <h3>Tartaria</h3>
        <p>sive</p>
        <h4>Magni Chami Imperium</h4>
        <p>Qui Tartaros describere velit, multas nationes describat necesse est, & easdem longe ab inuicem distantes.</p>
        <p>Nam Tartaria hodie fere vocatur, quicquid terrarum ab Orientali Oceano, sive Magnico, positum est inter Septentrionalem Oceanum & Meridionales regiones Sinam, Indiae partem extra Gangem, Sacos, Iaxartem (hodie Chesel) fluuium, mare Caspium &Meotidis paludem ; & ad Moscos pene Occidentem versus: qui haec omnia fere occupauerunt Tartari, & in iisdem locis residerunt.</p>
        <p>Ita ut antiquorum scriptorum Sarmatiam Asiaticam, utramque Scythiam, Sericamque regionem (quae fortasse hodie Cataio est) compraehendat.</p>
        <p> Horum nomen Anno salutis M.CC.XII. primum in Europa auditem est. In hordas dividuntur, quae vox illis significat conuentus, aut multitudo.</p>
        <p>Sed ut varias longe lateque distantes Provincias inhabitant, ita neque in moribus aut vitae genere omnes conueniunt.</p>
        <p>Homines statura quadrata, lata facie & obe sa, oculis contortis & concauis, sola barba horridi, cetera rasi, corpore valido, animo audaci; equis, aliisque animalibus quoquo modo interemptis suauiter vescuntur, demptis porcis, a quibus abstinent: inediae somnique patientissimi: equitantibus porro si fames sitisque illos molestauerit, equis quibus insident venas solent incidere, haustoque eorum sanguine famem pellunt. Et quoniam incertis vagantur sedibus, stellarum, imprimis vero Poli Arctici, quem ipsi sua lingu Selesnikol, hoc est, ferreum clauum (teste Sigismundo ab Herberstein,) vocant, aspectu, cursum suum dirigere solent. Uno in loco non diu commorantur, rati grauem esse inselicitatem diu in eodem loco haerere.</p>
        <p>Iustitia apud illos nulla.  Homines sunt rapacissimi, nempe pauperrimi; ut qui alienis semper inhiant.  Auri & argenti apud eos nullus usus.  Tartaros, eorumque mores & ritus accuratissime descriptos habes apud Sigismundum ab Herberstein. Vide & Antonij Bonfinij comentaria Hungarica; M. Paulum Venetum, quem diu inter illos vixisse constat.  De eorum origine lege Mathiam a Michou; Haythonum Armenum; Caelium Curionem in historia Sarracenica; & literas Iacobi Nauarchi Iesuitae.</p>
        <p>De Tartaris habes etiam quaedam non negligenda apud Nicephorum, libro 18. cap. 30.</p>
        <Wiktionary />
        <img src={map} />
        </>
    )
}