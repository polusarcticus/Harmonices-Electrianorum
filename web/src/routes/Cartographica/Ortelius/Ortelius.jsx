
import { useState, useEffect, useCallback } from 'react'
import { Link, Outlet  } from "react-router-dom";

export default function Ortelius() {
  return (
    <>
      <h1>Ortelius</h1>
      <Link to="theatrum-orbis-terrarum">Theatrum Orbis Terrarum</Link>
      <Outlet />
    </>)
}
