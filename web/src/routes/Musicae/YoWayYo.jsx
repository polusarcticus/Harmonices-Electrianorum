import * as Tone from 'tone'
import { useState, useEffect, Fragment } from 'react'
import styled from 'styled-components'

const Table = styled.div`
    display: grid;
    column-gap: 0.5em;
    grid-template-rows: repeat(12,1fr);
    grid-template-columns: 0.25fr 0.5fr 0.5fr 0.5fr 0.5fr 0.5fr;
`
const ChordsTable = styled.div`
    display: grid;
    column-gap: 0.5em;
    row-gap: 0.5em;
    grid-template-rows: repeat(4,1fr);
    grid-template-columns: 0.5fr 0.5fr 0.5fr 0.5fr 0.5fr 0.5fr;
`
function chordumdescriptio(root, numerator, denominator) {
  const arborisPrimae = primeFactors(root)
  const numeratorExplicitur = arborisPrimae.find((primum => primum === numerator))
  const denominatorExplicitur = arborisPrimae.find((primum => primum === denominator))
  const exactimum = {}

  if (!numeratorExplicitur || !denominatorExplicitur) {
    console.log('ratio approximatis', numerator, denominator, numerator/denominator)
  } else {
    //do exact
  }
  const descriptione = {
    root,
    numerator,
    denominator,
    ratio: numerator / denominator,
    ratioStr: `${numerator} / ${denominator}`,
    arborisPrimae,
  }

  console.log(descriptione)
}

function primeFactors(n) {
  const factors = [];
  let divisor = 2;
  while (n >= 2) {
    if (n % divisor == 0) {
      factors.push(divisor);
      n = n / divisor;
    } else {
      divisor++;
    }
  }
  return factors;
}


function equalTemperament(rootPitch, n) {
  return rootPitch * 2 ** (n / 12)
}

function perfectPitch(tonusradix, n) {
  const tone = rootPitch * 2 ** (n / 12)
  console.log('new / root', tone / rootPitch)
}

const fiveOddLimit = [{n:1, d:1}, {n:6, d:5}, {n:5,d:3}, {n:5,d:4}, {n:8,d:5}, {n:4,d:3}, {n:3,d:2}]

export default function YoWayYo() {
  const [basePitch, setBasePitch] = useState(440)

  // G = 1.122 ~ 9 / 8 = 3^2 * 2^-3
  // F = 1.259 ~ 5 / 4
  // C = 1.681 ~ 5 / 3
  // D = 1.498 ~ 3 / 2
  // A = 1.000 ~ 1 / 1
  // E = 1.334 ~ 4 / 3
  // bflat not found until 17 limit Bf = 1.8877 ~ 17/9
  // fsharp = 1.189 ~ 6/5
  // B = 1.781 ~ 16/9


  function chordioF() {
    const C3_8 = { f: [equalTemperament(basePitch, -21)], g: '8n' }
    const D3_8 = { f: [equalTemperament(basePitch, -19)], g: '8n' }
    const E3_8 = { f: [equalTemperament(basePitch, -17)], g: '8n' }
    const F3_8 = { f: [equalTemperament(basePitch, -16)], g: '8n' }
    const G3_8 = { f: [equalTemperament(basePitch, -14)], g: '8n' }
    const A3_8 = { f: [equalTemperament(basePitch, -12)], g: '8n' }
    const Bf3_8 = { f: [equalTemperament(basePitch, -23)], g: '8n' }
    const chordumF = [Bf3_8, G3_8, F3_8, C3_8, D3_8, A3_8, E3_8]
    const fiveOddLimitEq = []
    chordumF.forEach((note) => {
      console.log('note', note
      )
      console.log('220/note', 220/note.f[0])
    })
    console.log(chordumF)
  }
  //chordioF()
  function chordioG() {
    const C3_8 = { f: [equalTemperament(basePitch, -21)], g: '8n' }
    const D3_8 = { f: [equalTemperament(basePitch, -19)], g: '8n' }
    const E3_8 = { f: [equalTemperament(basePitch, -17)], g: '8n' }
    const Fs3_8 = { f: [equalTemperament(basePitch, -15)], g: '8n' }
    const G3_8 = { f: [equalTemperament(basePitch, -14)], g: '8n' }
    const A3_8 = { f: [equalTemperament(basePitch, -12)], g: '8n' }
    const B3_8 = { f: [equalTemperament(basePitch, -22)], g: '8n' }
    //const chordumF = [C3_8, D3_8, E3_8, Fs3_8, G3_8, A3_8, B3_8]
    const chordumF = [B3_8]
    const fiveOddLimitEq = []
    chordumF.forEach((note) => {
      console.log('note', note.f[0])
      console.log('220/note', 220/note.f[0])
    })
    //console.log(chordumF)
  }
  //chordioG()

  class ToneMatrix {
    constructor(basePitch){}
  }

  class LimitChord {
    constructor(name, basePitchs, numerators, denominators, length) {
      this.name = name
      this.basePitchs = basePitchs
      this.numerators = numerators
      this.denominators = denominators
      this._length = length
    }
    get frequency() {
      return this.numerators.map((num, i) => {
        return this.basePitchs[i] * (num/this.denominators[i])
      })
    }

    set length(newLength) {
      this._length = newLength
    }
    get length() {
      return this._length
    }
    get equation() {
      return this.numerators.map((n, i) => {
        return `${this.basePitchs[i]} * ${n}/${this.denominators[i]}`
      })
    }
  }

  class LimitNote {
    constructor(name, basePitch, numerator, denominator, length) {
      this.name
      this.basePitch = basePitch
      this.numerator = numerator
      this.denominator = denominator
      this._length = length
    }

    get frequency() {
      return [this.basePitch * (this.numerator/this.denominator)]
    }

    set length(newLength) {
      this._length = newLength
    }

    get length() {
      return this._length
    }

    get equation() {
      return `${this.basePitch}*${this.numerator}/${this.denominator}`
    }
  }

  class TETChord {
    constructor(name, basePitch, ns, length) {
      this.name = name
      this.basePitch = basePitch
      this.ns = ns
      this._length = length
    }
    get frequency() {
      return this.ns.map((n) => this.basePitch * 2**(n/12))
    }

    set length(newLength) {
      this._length = newLength
    }
    get length() {
      return this._length
    }
    get equation() {
      return this.ns.map((n) => {
        return `${this.basePitch}*2^(${n}/12)`
      })
    }
  }

  class TETNote {
    constructor(name, basePitch, n, length) {
      this.name = name
      this.basePitch = basePitch
      this.n = n
      this._length = length
    }

    get frequency() {
      return [this.basePitch * 2**(this.n/12)]
    }

    set length(newLength) {
      this._length = newLength
    }

    get length() {
      return this._length
    }

    get equation() {
      return `${this.basePitch}*2^(${this.n}/12)`
    }

  }

  const C3_8 = new TETNote('C3_8', basePitch, -21, '8n')
  const C3_4 = new TETNote('C3_4', basePitch, -21, '4n')
  const C3_2 = new TETNote('C3_2', basePitch, -21, '2n')

  const D3_8 = new TETNote('D3_8', basePitch, -19, '8n')

  const E3_8 = new TETNote('E3_8', basePitch, -17, '8n')
  const E3_2 = new TETNote('E3_2', basePitch, -17, '2n')

  const F3_8 = new TETNote('F3_8', basePitch, -16, '8n')

  const Fs3_8 = new TETNote('Fs3_8', basePitch, -15, '8n')

  const  G3_8 = new TETNote('G3_8', basePitch, -14, '8n')

  const  A3_8 = new TETNote('A3_8', basePitch, -12, '8n')

  const  Bf3_8 = new TETNote('Bf3_8', basePitch, -11, '8n')

  const  B3_8 = new TETNote('B3_8', basePitch, -10, '8n')

  const  C4_8 = new TETNote('C4_8', basePitch, -9, '8n')
  const  C4_4 = new TETNote('C4_4', basePitch, -9, '4n')
  const  C4_2 = new TETNote('C4_2', basePitch, -9, '2n')

  const  D4_8 = new TETNote('D4_8', basePitch, -7, '8n')
  const  D4_4 = new TETNote('D4_4', basePitch, -7, '4n')
  const  D4_2 = new TETNote('D4_2', basePitch, -7, '2n')

  const  E4_8 = new TETNote('E4_8', basePitch, -5, '8n')
  const  E4_4 = new TETNote('E4_4', basePitch, -5, '4n')
  const  E4_2 = new TETNote('E4_2', basePitch, -5, '2n')
  const  E4_1 = new TETNote('E4_1', basePitch, -5, '1n')


  const  F4_4 = new TETNote('F4_4', basePitch, -4, '4n')

  const  Fs4_8 = new TETNote('Fs4_8', basePitch, -3, '8n')
  const  Fs4_4 = new TETNote('Fs4_4', basePitch, -3, '4n')
  const  Fs4_2 = new TETNote('Fs4_2', basePitch, -3, '2n')

  const  G4_8 = new TETNote('G4_8', basePitch, -2, '8n')
  const  G4_4 = new TETNote('G4_4', basePitch, -2, '4n')

  const  A4_8 = new TETNote('A4_8', basePitch, 0, '8n')
  const  A4_6 = new TETNote('A4_6', basePitch, 0, '4n.')
  const  A4_4 = new TETNote('A4_4', basePitch, 0, '4n')
  const  A4_2 = new TETNote('A4_2', basePitch, 0, '2n')

  const  Bf4_8 = new TETNote('Bf4_8', basePitch, 1, '8n')
  const  Bf4_4 = new TETNote('Bf4_4', basePitch, 1, '4n')

  const  B4_6 = new TETNote('B4_6', basePitch, 2, '4n.')
  const  B4_4 = new TETNote('B4_4', basePitch, 2, '4n')
  const  B4_2 = new TETNote('B4_2', basePitch, 2, '2n')

  const  C5_4 = new TETNote('C5_4', basePitch, 3, '4n')

  const Rest = { frequency: null, length: '8n' }

  const G3B3D4_2 = new TETChord(
    'G3B3D4_2',
    basePitch,
    [-14, -10, -7],
    '2n'
  )
  const C3B3G3_2 = new TETChord(
    'C3B3G3_2',
    basePitch,
    [-21, -10, -14],
    '2n'
  )
  const E3G3B3_2 = new TETChord(
    'E3G3B3_2',
    basePitch,
    [-17, -14, -10],
    '2n'
  )

  const lineA = [
    { m: D4_4, h: Rest }, { m: A4_4, h: Rest }, { m: D4_2, h: Rest },
    { m: D4_4, h: Rest }, { m: A4_4, h: Rest }, { m: D4_2, h: Rest },
    { m: D4_4, h: D3_8 }, { m: Rest, h: F3_8 }, { m: A4_4, h: A3_8 }, { m: Rest, h: F3_8 },
    { m: D4_2, h: D3_8 }, { m: Rest, h: F3_8 }, { m: Rest, h: A3_8 }, { m: Rest, h: F3_8 },
    { m: F4_4, h: C3_8 }, { m: Rest, h: F3_8 }, { m: E4_8, h: A3_8 }, { m: D4_8, h: F3_8 },
    { m: C4_2, h: C3_8 }, { m: Rest, h: F3_8 }, { m: Rest, h: A3_8 }, { m: Rest, h: F3_8 },
    { m: D4_4, h: D3_8 }, { m: Rest, h: F3_8 }, { m: Bf4_4, h: Bf3_8 }, { m: Rest, h: F3_8 },
    { m: A4_6, h: D3_8 }, { m: Rest, h: A3_8 }, { m: Rest, h: C4_8 }, { m: G4_8, h: A3_8 },
    { m: F4_4, h: D3_8 }, { m: Rest, h: F3_8 }, { m: E4_8, h: A3_8 }, { m: D4_8, h: F3_8 },
    { m: E4_2, h: C3_8 }, { m: Rest, h: F3_8 }, { m: Rest, h: A3_8 }, { m: Rest, h: F3_8 },
  ]
  const lineB = [
    { m: E4_4, h: E3_8 }, { m: Rest, h: G3_8 }, { m: B4_4, h: B3_8 }, { m: Rest, h: G3_8 },
    { m: E4_2, h: E3_8 }, { m: Rest, h: G3_8 }, { m: Rest, h: B3_8 }, { m: Rest, h: G3_8 },
    { m: G4_4, h: C3_8 }, { m: Rest, h: E3_8 }, { m: Fs4_8, h: G3_8 }, { m: E4_8, h: E3_8 },
    { m: D4_2, h: D3_8 }, { m: Rest, h: Fs3_8 }, { m: Rest, h: A3_8 }, { m: Rest, h: Fs3_8 },
    { m: E4_4, h: E3_8 }, { m: Rest, h: G3_8 }, { m: C5_4, h: C4_8 }, { m: Rest, h: G3_8 },
    { m: B4_6, h: G3_8 }, { m: Rest, h: B3_8 }, { m: Rest, h: D4_8 }, { m: A4_8, h: B3_8 },
    { m: G4_4, h: C3_8 }, { m: Rest, h: E3_8 }, { m: Fs4_8, h: G3_8 }, { m: E4_8, h: E3_8 },
    { m: Fs4_2, h: D3_8 }, { m: Rest, h: Fs3_8 }, { m: Rest, h: A3_8 }, { m: Rest, h: Fs3_8 },
  ]
  const lineC = [
    { m: E4_4, h: E3_8 }, { m: Rest, h: G3_8 }, { m: C5_4, h: C4_4 }, { m: Rest, h: Rest },
    { m: B4_6, h: G3B3D4_2 }, { m: Rest, h: Rest }, { m: Rest, h: Rest }, { m: A4_8, h: Rest },
    { m: G4_4, h: C3B3G3_2 }, { m: Rest, h: Rest }, { m: Fs4_8, h: Rest }, { m: E4_8, h: Rest },
    { m: E4_1, h: E3G3B3_2 }, { m: Rest, h: Rest }, { m: Rest, h: Rest }, { m: Rest, h: Rest },
  ]
  const frequencibus = [...lineA, ...lineB, ...lineC]


  const oC3_8 = new LimitNote('C3_8', basePitch/2, 3, 5, '8n')
  const oD3_8 = new LimitNote('D3_8', basePitch/2, 2, 3, '8n')
  const oE3_8 = new LimitNote('E3_8', basePitch/2, 3, 4, '8n')
  const oF3_8 = new LimitNote('F3_8', basePitch/2, 4, 5, '8n')
  const oFs3_8 = new LimitNote('Fs3_8', basePitch/2, 5, 6, '8n')
  const oG3_8 = new LimitNote('G3_8', basePitch/2, 8, 9, '8n')
  const oA3_8 = new LimitNote('A3_8', basePitch/2, 1, 1, '8n')
  const oBf3_8 = new LimitNote('Bf3_8', basePitch/1, 9, 17, '8n')
  const oB3_8 = new LimitNote('B3_8', basePitch/1, 9, 16, '8n')

  const oC4_8 = new LimitNote('C4_8', basePitch/1, 3, 5, '8n')
  const oC4_4 = new LimitNote('C4_4', basePitch/1, 3, 5, '4n')
  const oC4_2 = new LimitNote('C4_2', basePitch/1, 3, 5, '2n')

  const oD4_8 = new LimitNote('D4_8', basePitch/1, 2, 3, '8n')
  const oD4_4 = new LimitNote('D4_4', basePitch/1, 2, 3, '4n')
  const oD4_2 = new LimitNote('D4_2', basePitch/1, 2, 3, '2n')

  const oE4_8 = new LimitNote('E4_8', basePitch/1, 3, 4, '8n')
  const oE4_4 = new LimitNote('E4_4', basePitch/1, 3, 4, '4n')
  const oE4_2 = new LimitNote('E4_2', basePitch/1, 3, 4, '2n')
  const oE4_1 = new LimitNote('E4_1', basePitch/1, 3, 4, '1n')

  const oF4_8 = new LimitNote('F4_8', basePitch/1, 4, 5, '8n')
  const oF4_4 = new LimitNote('F4_4', basePitch/1, 4, 5, '4n')

  const oFs4_8 = new LimitNote('Fs4_8', basePitch/1, 5, 6, '8n')
  const oFs4_4 = new LimitNote('Fs4_4', basePitch/1, 5, 6, '4n')
  const oFs4_2 = new LimitNote('Fs4_2', basePitch/1, 5, 6, '2n')

  const oG4_8 = new LimitNote('G4_8', basePitch/1, 8, 9, '8n')
  const oG4_4 = new LimitNote('G4_4', basePitch/1, 8, 9, '4n')

  const oA4_8 = new LimitNote('A4_8', basePitch/1, 1, 1, '8n')
  const oA4_6 = new LimitNote('A4_6', basePitch/1, 1, 1, '4n.')
  const oA4_4 = new LimitNote('A4_4', basePitch/1, 1, 1, '4n')
  const oA4_2 = new LimitNote('A4_2', basePitch/1, 1, 1, '2n')

  const oBf4_4 = new LimitNote('Bf4_4', basePitch*2, 9, 17, '4n')

  const oB4_6 = new LimitNote('B4_6', basePitch*2, 9, 16, '4n.')
  const oB4_4 = new LimitNote('B4_8', basePitch*2, 9, 16, '4n')

  const oC5_4 = new LimitNote('C5_4', basePitch*2, 3, 5, '4n')
  // key change, net notes

  const oRest = { frequency: null, length: '8n' }
  const codexLimit = [
    oC4_4, oD4_4, oE4_4, oF4_4, oFs4_4, oG4_4, oA4_4, oBf4_4, oB4_4
  ]

  const oG3B3D4_2 = new LimitChord(
    'oG3B3D4_2',
    [basePitch/2, basePitch, basePitch],
    [8, 9, 2],
    [9, 16, 3],
    '2n'
  )
  /*
  const oG3B3D4_2 = {
    frequency: [
      (basePitch/2) * 8/9,
      (basePitch/1) * 9/16,
      (basePitch/1) * 2/3
    ],
    length: '2n'
  }
  */
  const  oC3B3G3_2 = new LimitChord(
    'oC3B3G3_2',
    [basePitch/2, basePitch, basePitch/2],
    [3, 9, 8],
    [5, 16, 9],
    '2n'
  )
  /*
  const oC3B3G3_2 = {
    frequency: [
      (basePitch/2) * 3/5,
      (basePitch/1) * 9/16,
      (basePitch/2) * 8/9
    ],
    length: '2n'
  }
  */
  const  oE3G3B3_2 = new LimitChord(
    'oE3G3B3_2',
    [basePitch/2, basePitch/2, basePitch/1],
    [3, 8, 9],
    [4, 9, 16],
    '2n'
  )
  /*
  const oE3G3B3_2 = {
    frequency: [
      (basePitch/2) * 3/4,
      (basePitch/2) * 8/9,
      (basePitch/1) * 9/16,
    ],
    length: '2n'
  }
  */

  const lineAOddLimit = [
    { m: oD4_4, h: oRest }, { m: oA4_4, h: oRest }, { m: oD4_2, h: oRest },
    { m: oD4_4, h: oRest }, { m: oA4_4, h: oRest }, { m: oD4_2, h: oRest },
    { m: oD4_4, h: oD3_8 }, { m: oRest, h: oF3_8 }, { m: oA4_4, h: oA3_8 }, { m: oRest, h: oF3_8 },
    { m: oD4_2, h: oD3_8 }, { m: oRest, h: oF3_8 }, { m: oRest, h: oA3_8 }, { m: oRest, h: oF3_8 },
    { m: oF4_4, h: oC3_8 }, { m: oRest, h: oF3_8 }, { m: oE4_8, h: oA3_8 }, { m: oD4_8, h: oF3_8 },
    { m: oC4_2, h: oC3_8 }, { m: oRest, h: oF3_8 }, { m: oRest, h: oA3_8 }, { m: oRest, h: oF3_8 },
    { m: oD4_4, h: oD3_8 }, { m: oRest, h: oF3_8 }, { m: oBf4_4, h: oBf3_8 }, { m: oRest, h: oF3_8 },
    { m: oA4_6, h: oD3_8 }, { m: oRest, h: oA3_8 }, { m: oRest, h: oC4_8 }, { m: oG4_8, h: oA3_8 },
    { m: oF4_4, h: oD3_8 }, { m: oRest, h: oF3_8 }, { m: oE4_8, h: oA3_8 }, { m: oD4_8, h: oF3_8 },
    { m: oE4_2, h: oC3_8 }, { m: oRest, h: oF3_8 }, { m: oRest, h: oA3_8 }, { m: oRest, h: oF3_8 },
  ]
  const lineBOddLimit = [
    { m: oE4_4, h: oE3_8 }, { m: oRest, h: oG3_8 }, { m: oB4_4, h: oB3_8 }, { m: oRest, h: oG3_8 },
    { m: oE4_2, h: oE3_8 }, { m: oRest, h: oG3_8 }, { m: oRest, h: oB3_8 }, { m: oRest, h: oG3_8 },
    { m: oG4_4, h: oC3_8 }, { m: oRest, h: oE3_8 }, { m: oFs4_8, h: oG3_8 }, { m: oE4_8, h: oE3_8 },
    { m: oD4_2, h: oD3_8 }, { m: oRest, h: oFs3_8 }, { m: oRest, h: oA3_8 }, { m: oRest, h: oFs3_8 },
    { m: oE4_4, h: oE3_8 }, { m: oRest, h: oG3_8 }, { m: oC5_4, h: oC4_8 }, { m: oRest, h: oG3_8 },
    { m: oB4_6, h: oG3_8 }, { m: oRest, h: oB3_8 }, { m: oRest, h: oD4_8 }, { m: oA4_8, h: oB3_8 },
    { m: oG4_4, h: oC3_8 }, { m: oRest, h: oE3_8 }, { m: oFs4_8, h: oG3_8 }, { m: oE4_8, h: oE3_8 },
    { m: oFs4_2, h: oD3_8 }, { m: oRest, h: oFs3_8 }, { m: oRest, h: oA3_8 }, { m: oRest, h: oFs3_8 },
  ]
  const lineCOddLimit = [
    { m: oE4_4, h: oE3_8 }, { m: oRest, h: oG3_8 }, { m: oC5_4, h: oC4_4 }, { m: oRest, h: oRest },
    { m: oB4_6, h: oG3B3D4_2 }, { m: oRest, h: oRest }, { m: oRest, h: oRest }, { m: oA4_8, h: oRest },
    { m: oG4_4, h: oC3B3G3_2 }, { m: oRest, h: oRest }, { m: oFs4_8, h: oRest }, { m: oE4_8, h: oRest },
    { m: oE4_1, h: oE3G3B3_2 }, { m: oRest, h: oRest }, { m: oRest, h: oRest }, { m: oRest, h: oRest },
  ]
  const frequencibusOddLimit = [...lineAOddLimit, ...lineBOddLimit, ...lineCOddLimit]

  const codexChordumET = [G3B3D4_2, C3B3G3_2, E3G3B3_2]
  const codexChordumLimit = [oG3B3D4_2, oC3B3G3_2, oE3G3B3_2]
  const codexET = [ C4_4, D4_4, E4_4, F4_4, Fs4_4, G4_4, A4_4, Bf4_4, B4_4]


  const ChordLines = codexChordumET.map((chord, i) => {
    return (
      <Fragment key={i}>
        <div style={{
          gridRow: i+2,
          gridColumn:1
        }}>{chord.name}</div>
        <div style={{
          gridRow: i+2,
          gridColumn:2,
          display: 'grid',
          gridTemplateColumns: '1fr',
          gridTemplateRows: 'repeat(3, 1fr)'
        }}>{chord.equation.map((eq,i) => (<div key={i}>{eq}</div>))}</div>
        <div style={{
          gridRow: i+2,
          gridColumn:3
        }}>
          <button onClick={() => {
            const tone = new Tone.PolySynth(Tone.Synth).toDestination();
            const now = Tone.now()
            tone.triggerAttackRelease(chord.frequency, chord.length, now)
          }}>{chord.frequency.map((note, i) => {
            return (<Fragment key={i}>{note.toFixed(6)}<br/></Fragment>)
          })}</button>
        </div>
        <div style={{
          gridRow: i+2,
          gridColumn:4,
          display: 'grid',
          gridTemplateColumns: '1fr',
          gridTemplateRows: 'repeat(3, 1fr)'
        }}>{codexChordumLimit[i].equation.map((eq,i) => {
          return (<div key={i}>{eq}</div>)
          })
        }
        </div>
        <div style={{
          gridRow: i+2,
          gridColumn:5
        }}>
          <button onClick={() => {
            const tone = new Tone.PolySynth(Tone.Synth).toDestination();
            const now = Tone.now()
            tone.triggerAttackRelease(codexChordumLimit[i].frequency, chord.length, now)
          }}>{codexChordumLimit[i].frequency.map((note, i) => {
            return (<Fragment key={i}>{note.toFixed(6)}<br/></Fragment>)
          })}</button>
        </div>

        <div style={{
          gridRow: i+2,
          gridColumn:6
        }}>
          <button onClick={() => {
            const tone = new Tone.PolySynth(Tone.Synth).toDestination();
            const now = Tone.now()
            tone.triggerAttackRelease(chord.frequency, chord.length, now)
            tone.triggerAttackRelease(codexChordumLimit[i].frequency, codexChordumLimit[i].length, now + 1)
            tone.triggerAttackRelease([...codexChordumLimit[i].frequency, ...chord.frequency], chord.length, now + 2)

          }}>{
            chord.frequency.map((note,j) => {
              return (<Fragment key={j}>
                {(note - codexChordumLimit[i].frequency[j]).toFixed(6)}<br/></Fragment>)
            })
          }</button>
        </div>
      </Fragment>
    )
  })

  const Lines = codexET.map((note,i) => {
    return (<Fragment key={i}>
      <div style={{
        gridRow: i+2,
        gridColumn:1
      }}>{note.name}</div>
      <div style={{
        gridRow: i+2,
        gridColumn:2
      }}>{note.equation}</div>
      <div style={{
        gridRow: i+2,
        gridColumn:3
      }}>
        <button onClick={() => {
          const tone = new Tone.PolySynth(Tone.Synth).toDestination();
          const now = Tone.now()
          tone.triggerAttackRelease(note.frequency, note.length, now)
        }}>{note.frequency[0].toFixed(6)}</button>
      </div>
      <div style={{
        gridRow: i+2,
        gridColumn:4
      }}>{codexLimit[i].equation}</div>
      <div style={{
        gridRow: i+2,
        gridColumn:5
      }}>
        <button onClick={() => {
          const tone = new Tone.PolySynth(Tone.Synth).toDestination();
          const now = Tone.now()
          tone.triggerAttackRelease(codexLimit[i].frequency, codexLimit[i].length, now)
        }}>{codexLimit[i].frequency[0].toFixed(6)}</button>
      </div>
      <div style={{
        gridRow: i+2,
        gridColumn:6
      }}>
        <button onClick={() => {
          const tone = new Tone.PolySynth(Tone.Synth).toDestination();
          const now = Tone.now()
          tone.triggerAttackRelease(note.frequency, note.length, now)
          tone.triggerAttackRelease(codexLimit[i].frequency, codexLimit[i].length, now + 1)
          tone.triggerAttackRelease([codexLimit[i].frequency, note.frequency], codexLimit[i].length, now + 2)

        }}>{(note.frequency - codexLimit[i].frequency[0]).toFixed(6)}</button>
      </div>
    </Fragment>)
    console.log(Lines)
  })

  return (
    <>
      <input value={basePitch} type="text" name="chordum vitae" onChange={e => setBasePitch(e.target.value)} />
      <button onClick={() => {
        const melody = new Tone.PolySynth(Tone.Synth).toDestination();
        const harmony = new Tone.PolySynth(Tone.Synth).toDestination();
        const now = Tone.now()
        frequencibus.forEach(({ m, h }, i) => {
          melody.triggerAttackRelease(m.frequency, m.length, now + (i / 2))
          harmony.triggerAttackRelease(h.frequency, h.length, now + (i / 2))
        })
      }
        }>
        Yo Way Yo (ET)</button><br/>
      <button onClick={() => {
        const melody = new Tone.PolySynth(Tone.Synth).toDestination();
        const harmony = new Tone.PolySynth(Tone.Synth).toDestination();
        const now = Tone.now()
        frequencibusOddLimit.forEach(({ m, h }, i) => {
          melody.triggerAttackRelease(m.frequency, m.length, now + (i / 2))
          harmony.triggerAttackRelease(h.frequency, h.length, now + (i / 2))
        })


      }}>Yo Way Yo (sectio primus) (select odd limit)</button>
      <h2>Comparo</h2>
      <Table>
        <div style={{
          gridRow: 1,
          gridColumn:1
        }}>Chorda</div>
        <div style={{
          gridRow: 1,
          gridColumn:2
        }}>ET</div>
        <div style={{
          gridRow: 1,
          gridColumn:3
        }}>Hertz</div>
        <div style={{
          gridRow: 1,
          gridColumn:4
        }}>Limit</div>
        <div style={{
          gridRow: 1,
          gridColumn:5
        }}>Hertz</div>
        <div style={{
          gridRow: 1,
          gridColumn:6
        }}>Difference</div>
        {Lines}
      </Table>
      <ChordsTable>
        <div style={{
          gridRow: 1,
          gridColumn:1
        }}>Chordae</div>
        <div style={{
          gridRow: 1,
          gridColumn:2
        }}>ET</div>
        <div style={{
          gridRow: 1,
          gridColumn:4
        }}>Limit</div>
        <div style={{
          gridRow: 1,
          gridColumn:6
        }}>Difference</div>
        {ChordLines}
      </ChordsTable>


    </>
  )
}
