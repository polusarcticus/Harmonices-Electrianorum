import { redirect, useLocation } from "react-router-dom";
import { useEffect } from 'react'
export const WiktionaryRedirector = () => {
    const location = useLocation()
    console.log(location)

    useEffect(() => {
        window.location = `https://en.wiktionary.org${location.pathname}${location.hash}`
    }, [])
    return (<>Redirecting to Wiktionary...</>)
}