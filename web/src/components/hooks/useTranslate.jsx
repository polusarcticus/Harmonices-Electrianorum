
import { useState, useEffect, useCallback } from 'react'
import axios from 'axios';

export const useTranslate = () => {
    const [htmls, setHtmls] = useState([''])
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)
    const [words, setWords] = useState([''])

    const fetchTranslate = useCallback(async (newWord) => {
        setLoading(true)
        try {
            const response = await axios.get(
                'https://en.wiktionary.org/w/api.php',
                {
                    params: {
                        action: 'parse',
                        page: newWord,
                        prop: 'text',
                        formatversion: 2,
                        origin: '*',
                        format: 'json'
                    }
                }
            )
            console.log('response', response)
            if (response.data.hasOwnProperty("error")) {
                setHtmls(current => [...current, `<p>${response.data.error.info}</p>`])
                setWords(current => [...current, newWord])
                setError(response.data.error.info)
            } else if (response.data.hasOwnProperty("parse")) {
                setHtmls(current => [...current, response.data.parse.text])
                setWords(current => [...current, newWord])
            } else {
                console.log(response)
                setHtmls(current => [...current, "<p>no responsefound</p>"])
                setWords(current => [...current, newWord])
            }
        } catch (e) {
            console.log(e)
            setError(e)
            setHtmls(current => [...htmls, "<div> e unfortunate</div>"])
        }
        setLoading(false)
    }, [])


    return { htmls, words, fetchTranslate, loading }
}