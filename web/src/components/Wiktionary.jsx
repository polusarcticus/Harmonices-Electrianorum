import { useState, useCallback, useEffect } from 'react'

import axios from 'axios';
import parse from 'html-react-parser'
import '@/components/hooks/useTranslate'
import { useTranslate } from '@/components/hooks/useTranslate';

export default function Wiktionary() {
    const [x,setX] = useState(0)
    const [y,setY] = useState(0)

    const [isPopoverOpen, setIsPopoverOpen] = useState(false)
    const [enableTranslate, setEnableTranslate] = useState(false)
    const [text, setText] = useState('')
    const { htmls, words, fetchTranslate, loading } = useTranslate()
    const [currentHtml, setCurrentHtml] = useState('')
    const [response, setResponse] = useState('')

    useEffect(() => {
        window.addEventListener("mouseup", (evt) => {
            setX(evt.pageX)
            setY(evt.pageY)
            setText(window.getSelection().toString()) 
        }) 
        return () => {
            window.removeEventListener("mouseup", () => {console.log('rm wikitionary evt listener')})
        }
    }, [])

    useEffect(() => {
        if (!isPopoverOpen) {
            setIsPopoverOpen(true)
        }
    }, [text])


    return (
        <div style={{
        }}>
            {enableTranslate ?
                (<div
                    style={{
                        backgroundColor: 'yellow',
                        position: 'sticky',
                        top: `${y}px`,
                        left: `${x}px`,
                    }}
                >
                    <div style={{
                        position: 'absolute',
                        top: '0px',
                        right: '0px'
                    }}
                        onClick={() => {
                            setEnableTranslate(!enableTranslate)
                        }}
                    >Close</div>
                    {loading && (<p>Loading...</p>)}
                    {htmls && (<div>
                        <div>{words.map((word, i) => {
                            return(<div key={i}
                            onClick={() => {
                                setCurrentHtml(htmls[i])
                            }} 
                            style={{
                                padding: '1vh 1vw',
                                border: 'white 1px solid',
                                display: 'inline',
                                margin: '0 auto',
                                textAlign: 'center',
                                backgroundColor: 'grey',
                                color: i == currentHtml ? 'blue' :'yellow'
                            }}>{word}</div>)
                        })

                        }
                            </div>{parse(currentHtml)}</div>
                    )}
                </div>) :
                (<>

                </>)
            }
            <div>
                {text ?
                    (<div
                        style={{
                            position: 'absolute',
                            top: `${y - 100}px`,
                            left: `${x - 100}px`,
                            backgroundColor: 'blue',
                            width: '100px',
                            height: '100px'
                        }}
                    >
                        <div onClick={() => setIsPopoverOpen(!isPopoverOpen)}>Close</div>
                        <button
                            onClick={() => {
                                setEnableTranslate(true)
                                console.log('query text', text)
                                fetchTranslate(text)
                            }}>Translate Selection</button>
                    </div>) :
                    <></>
                }
            </div>
        </div>)
}